package Misc;

import java.util.Scanner;

/*
 * Przyk�ad klasy abstrakcyjnej (wirtualnej). Klasy tego typu
 * nie mog� by� bezpo�rednio typem zmiennych, nie mog� te�
 * bezpo�rednio zosta� powo�ane do pami�ci operacyjnej w
 * programie. Mog� by� jednak wykorzystywane np. do rozszerzania
 * mo�liwo�ci i funkcjonalno�ci innych klas, kt�re b�d� mog�y
 * korzysta� z ich metod czy sk�adowych
 */

public abstract class IO {
	//protected gwarantuje, �e tylko klasa rozszerzana
	//b�dzie mog�a uzyska� dost�p do metody
	protected void print(Object o) {
		System.out.print(o);
	}
	
	protected void println(Object o) {
		print(o+"\n");
	}
	//wy��czenie ostrze�enia o nie zamkni�tym zr�dle 
	//wej�ciowym
	@SuppressWarnings("resource")
	protected Object write() {
		return ((new Scanner(System.in)).next());
	}
	
	//"przeci��enie" metody pozwalaj�ce bezpo�rednio pozyska�
	//ci�g znakowy od u�ytkownika (o pe�nym przeci��eniu 
	//nie mo�na m�wi�, gdy� nazwy metod s� r�ne).
	protected String writeStr()	{
		return String.valueOf(write());
	}
	
	protected int writeInt() {
		//�eby nie pisa� dwa razy bloku try...catch
		return  (int)writeDouble();//Integer.valueOf(String.valueOf(writeDouble()));
	}
	
	protected double writeDouble() {
		//poni�sza klauzula try..catch gwarantuje, �e program
		//nie zako�czy si� zb��dem gdy u�ytkownik poda warto��
		//inn� ni� liczb� rzeczywist�. W takim wypadku zostanie
		//zwr�cone 0.
		try {
			return Double.valueOf(writeStr());
		}
		catch (Exception ex) {
			//nie instejeruje nas b��d... po prostu wysz�o CO� zamiast liczby i wystawiamy 0.0
			return 0;
		}
	}
	
	
 }

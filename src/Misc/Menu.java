package Misc;

import java.util.Map;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import Controller.*;

/* 
 * Klasa zawiera uniwersalny spos�b tworzenia menu programu. Dzi�ki niej jedyne, gdzie musimy ingerowa�
 * to lista opcji menu. W p�zniejszym kodzie automatycznie wy�wietlane jest menu tak jak rozpisana jest
 * lista (nie po numerach naszej mapy, kt�re to mog� by� wypisane NIE W KOLEJNO�CI).
 * Ponadto w warto�ci mapy podajemy nazw� wywo�ywanej metody na wybranie przez u�ytkownika 
 * konkretnej opcji w menu. 
 * Klasa zosta�a rozszerzona o abstrakcyjn� (wirtuln�) klas� 
 * pozwalaj�c� na ujednolicenie odczytu strumienia wej�cia (
 * (konsola u�ytkownika) jak i wyprowadzeniu tesktu (funkcje
 * write i writeln)
 * Klasa IO zosta�a napisana pod ten projekt (jest w poliku IO.java)
 */
public class Menu extends IO{
	/* Zastosowanie Map<?, List<String> > daje mo�liwo�� utworzenia
	 * elastycznego tzw. kontenera (kub�a), w kt�rym mo�emy tworzy� 
	 * klucze wedle dowolnej zmiennej (znak zapytania wskazuje,
	 * �e naszym kluczem w mapie mo�e by� zar�wno liczba jak i 
	 * ci�g znakowy. 
	 * Warto�ci mapy zapisywane s� jako lista ci�g�w znakowych,
	 * przez co mo�emy zapisa� wi�cej ni� jeden ci�g znakowy 
	 * pod danym kluczem. W naszym wypadku potrzebne s� dok�adnie
	 * dwa ci�gi:
	 * - pierwszy to nazwa p�zniej wywo�ywanej funkcji, je�eli
	 * u�ytkownik wybierze okre�lon� opcj�
	 * - drugi to nazwa opcji  wy�wietlana dla u�ytkownika w menu
	 * 
	 * adnotacja SuppressWarnings zosta�a dodana ze wzgl�du
	 * na wy�wietlane ostrze�enia o braku uszeregowania naszej zmiennej
	 * mapowej
	 */
	@SuppressWarnings("serial")
	Map<?,List<String> > menu = new LinkedHashMap<>() {{
									//kluczami s� warto�ci int, za� list� warto�ci
									//tworzymy przez bezpo�rednie dodanie dw�ch ci�g�w znakowych
									//do listy ci�g�w znakowych
		
									//UWAGA! Warto�ci kluczy musz� by� unikatowe
									//jednak nie musz� one wyst�powa� w postaci uporz�dkowanej
									put(1, new ArrayList<>() {{add("add");add("Dodaj");}}); 
									put(2, new ArrayList<>() {{add("modify"); add("Modyfikuj");}});
									put(3, new ArrayList<>() {{add("show"); add("Wy�wietlenie");}});
									put(5, new ArrayList<>() {{add("delete"); add("Usuni�cie");}});
									//put(6, new ArrayList<>() {{add("search"); add("Szukanie");}});
									put(4, new ArrayList<>() {{add("end"); add("Zako�czenie");}});
						}};
	Variables test;	
						
	public Menu() {};
	
	public Menu(Variables test) {this.test=test; println(this.test.getPrivString());}
	
    /*
     * Metoda ma za zadanie utworzy� menu na podstawie
     * wcze�niej oopisanej zmiennej menu
     */
	@SuppressWarnings("rawtypes")
	public void createMenu() {
		int pos=1; //zmienna-liczba, kt�ra b�dzie liczb� porz�dkow� w menu
		//for (Object s: menu) {
		//nie ma r�nicy numeracja w mapie i numeracja pod pos - tylko b�dziemy pozniej sprawdza�
		//czy ewentualnie jest jakikolwiek wpis z okre�lonym numerem
		for(;;) { //tutaj jest przeniesiona p�tla
			pos=1;
			for (List s: menu.values()) {
				//przy wy�wietlaniu menu warto�� pos b�dzie
				//zwi�kszana o jeden; za� p�tla foreach wybiera
				//jedna po drugiej warto�� z menu i wy�wietla
				//drug� pozycj� z listy ci�g�w znakowych
				println((pos++) + ". " +  s.get(1));
			}
			//p�tla wieczna - po wypisaniu menu poni�szy for
			//zap�tli program i ka�e "do�ywotnio" wykonywa� 
			//metod� getChoise();
			//PETLA PRZENIESIONA - INACZEJ WYBOR WYSWIETLALBY SIE BEZ MENU!
			 getChoise();
		}
	}
	
	private void getChoise() {
		int v;
		//p�tla wieczna pobieraj�ca wyb�r od u�ytkownika
		//je�eli u�ytkownik nie poda odpowiedniej warto�ci
		//b�dzie otrzymywa� komunikat o z�ym wyborze
		//je�eli za� wybierze odpowiedni� (istniej�c�) opcj�
		//to p�tla ta zostanie przerwana
		for(;;) {
			print("Wybrano: ");
			//istotne - przypisujemy tutaj warto�� v - czyli opcj� menu wybran� przez naszego u�ytwkonika
			//tutaj u�ywamy autorskiej funkcji writeInt() - metoda pobiera
			//od u�ytkownika warto�� liczbow� i zwraca j�
			if (menu.containsKey(v=writeInt())) {
				//je�eli opcja istnieje na li�cie - przerywamy wieczyst� p�tl�
				break;
			}
			//je�eli u�ytkownik wpisa� cokolwiek innego...
			println("Podano nieistniej�c� opcj�. Spr�buj ponownie...");
		}
		//zmieniamy klucze mapy na tablic� element�w o type Object
		Object[] kk = menu.keySet().toArray();
		//maj�c ju� klucze, wykonujemy p�tl� for po wszystkich 
		//elementach by znalez� ten w�a�ciwy.
		//for each by�by tutaj nieprzydatny, bo nie pozwoli�by 
		//przerwa� swojej p�tli w dowolnym momencie
		for (int i=0;i<kk.length;i++) {
			//tutaj sprawdzamy, jaki klucz posiada wybrana przez nas opcja
			//kolejno przegl�damy warto�ci wybranej przez nas opcji 
			//je�eli trafimy na odpowiedni� warto�� - przechodzimy do wywo�ania odpowiedniej metody...
			if ((int)kk[v-1]==(i+1)) {
				//tworzmy obiekt z klasy odpowiadaj�cej za oeprcje na naszym zbiorze gier...
				Operations operation = new Operations(/*test*/);
				//tworzymy zmienn� klasy Method, pozwalaj�cej na wykonywanie metod z okre�lonych obiekt�w
				//na podstawie nazwy metody zapisanej w ci�gu znakowym 
				Method launch;
				try {
					//odnajdujemy nasz� metod�... jej nazw� w naszej zmiennej menu
					launch = operation.getClass().getMethod(menu.get(i+1).get(0));
					//i nakazujemy si� jej wywo�a�
					launch.invoke(operation);
					//przerywamy p�tl� - wszystko zadzia�a�o.
					break;
				}
				catch (Exception ex) {
					//nic nie robimy - nie interesuje nas niepowodzenie!
				}
			}
		}
	}
}

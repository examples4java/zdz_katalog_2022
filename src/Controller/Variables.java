package Controller;

import java.util.ArrayList;
import java.util.List;

/*
 * Klasa ta ma za zadanie przechowywa� wszystkie zmienne
 * programu, kt�re b�d� wykorzystywane w trakcie jego u�ytkowania.
 * Dost�p do przechowywanych zmiennych przez nasz� klas�
 * musi mie� zakres GLOBALNY, czyli ka�da, dowolna klasa, a tym
 * samym obiekt naszego programu, musi miec mo�liwo�� zapisu
 * i odczytu zadeklarowanyc i zdefiniowanych tutaj zmiennych.
 * 
 * Klasa dosta�a dodatkowych modyfikator final by nikt nie 
 * wykorzysta� jej do utworzenia nowej klasy (�eby nie u�y� 
 * jej jako bazowej - ze s�owem extends)
 * 
 * Klasa zawiera wszystkie zmienne jako prywatne, tj. tylko ta
 * klasa i jej metody mo�e je bez limitu modyfikowa�. Pozosta�e
 * klasy mog� tylko ewentualnie robi� to, co przewidzia�
 * autor tej klasy.
 */

public final class Variables extends Misc.IO{
	/*
	 * JEDYNA STATYCZNA PRYWATNA ZMIENNA tej klasy.
	 * Jej zadaniem jest przechowywa� obiekt klasy Variables.
	 * Jak mo�na si� domy�le�, static powoduje, �e zmienna
	 * b�dzie istnie� nawet w przypadku, gdy �aden obiekt o 
	 * klasie Variables nie zostanie utworzony w naszym programie.
	 * Dzi�kie temu dzia�aniu zmienna v tak naprawd� istnieje
	 * od pocz�tku dzia�ania programu (od pierwszej linii kodu
	 * funkcji main) do samego zako�czenia programu (czyli
	 * wyczyszczenia przerzeni pami�ci z jakiejkolwiej jego
	 * dzia�alno�ci).
	 */
	private static Variables v;
	
	//poni�ej zosta�y zdeklarowane zmienne, do kt�rych dost�p
	//realnie b�dzie dzia�a� na ka�dym poziomie naszego 
	//programu (w ka�dej klasie). Do tych zmiennych mo�na
	//utworzy� dowolne metody pobierania i zapisywania - tak
	//jak ma to miejsce w ka�dej innej klasie przy tworzeniu
	//zmiennych oraz metod set i get
	//nas intresuje jedynie zmienna games, pozosta�e to zmienne
	//dodane w czasie testowania klasy i nie maj� �adnego
	//znaczenia dla dzia�ania perogramu
	private List<Games.Game> games;

	String privString;
	static int instCout = 1;
	
	//publiczny konstruktor klasy; ka�de jego wywo�anie 
	//zwi�ksza warto�� instCount o jeden, dzi�ki czemu mo�na
	//obserwowa�, czy pracujemy na okre�lonej zmiennej, czy na
	//jej kolejnej kopii (wtedy, gdy odpytamy instancj�
	//zmiennej typu Variables i dostaniemy w odpiwiedzi np. w
	//klasie Operations warto�� 1, a w Game warto�c 2 to b�dziemy
	//wiedzie�, �e nie pracujemy na tym samym obiekcie (gdzie�
	//obie warto�ci si� roz��czy�y).
	//obecnie dzia�anie to i tak jest pomijane 
	public Variables() {privString="INSTANCJA " + instCout++;}
	
	public void setPrivString(String s) {privString=s;}
	
	public String getPrivString() {return privString;}
	
	//metoda pozwala doda� do zmiennej games now� gr�
	//(zmienn� klasy Game)
	public void addGame(Games.Game game) {
		//println("JESTEM TUTAJ:");
		if (games==null) //je�eli lista games nie istnieje...
			games = new ArrayList<>(); //utw�rzmy j�
		games.add(game); //dodanie kolejnej/pierwszej gry
	}
	
	//metoda zwraca ca�� list�, ze wszystkimi zawartymi grami
	public List<Games.Game> getGames() {
		return games;
	}
	
	//metoda ma za zadanie zwr�ci� WSZYSTKIE zmienne zapisane w
	//obiekcie (wszystkie publiczne). Poniewa� nie mamy pewno�ci
	//czy zwracana zmienna (v) b�dzie ju� zainicjalizowana w
	//programie czy dopiero chcemy ja zainicjalizowa� (utworzy�
	//po raz pierwszy w pami�ci) dlatego metoda jest typu static
	//(istnieje przez ca�y okres istnienia programu, tak samo
	//zreszt� jak znmienna v). 
	public static Variables getVariables() {
		//operator tr�jstanowy (ternary operator) sprawdza, czy
		//zmienna v jest null (pusta, nieinicjalizowana). Je�eli
		//taka b�dzie - tworzymy nowy obiekt Variables(przypisujemy
		//go do zmiennej v, kt�ra w kolejnym kroku zostaje
		//zr�cona jako warto�� przez metod�). Je�eli za� zmienna
		//ju� istnieje to po prostu zwracamy warto�� v.
		return v==null ? v=new Variables() : v;
	}
}

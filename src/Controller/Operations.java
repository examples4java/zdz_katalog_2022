package Controller;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import Games.Game;

public final class Operations extends Misc.IO {
	
	private Variables test;
	
	public Operations() {};
	
	public Operations(Variables test) {this.test=test; println(this.test.getPrivString());}
	
	
	public void add() {
		print("Podaj nazw� dodawanej gry: ");
		Game game = Game.setName(writeStr());
		//Game game = new Game("TESTOWA GRA");
		print("Podaj nazw� skr�con�: " );
		game.setShortName(writeStr());
		print("Podaj opis: " );
		game.setDescription(writeStr());
		print("Podaj dat� publikacji: " );
		game.setPublishDate(writeStr());
		print("Podaj platform�: " );
		game.setPlatform(writeStr());
		print("Podaj czas uko�czenia (min): " );
		game.setGameplayTime(writeInt());
		for(;;) {
			String v;
			print("Podaj tw�rc�: " );
			game.addCreator(v=writeStr());
			print("Doda� nast�pnego (T/n)? ");
			v=writeStr();
			if (v.charAt(0) == 'n' || v.charAt(0) == 'N') break;
		}
		for(;;) {
			String v;
			print("Podaj wydawc�: " );
			game.addPublisher(v=writeStr());
			print("Doda� nast�pnego (T/n)? ");
			v=writeStr();
			if (v.charAt(0) == 'n' || v.charAt(0) == 'N') break;
		}
		for(;;) {
			String v;
			print("Podaj gatunek: " );
			game.addGenre(v=writeStr());
			print("Doda� nast�pnego (T/n)? ");
			v=writeStr();
			if (v.charAt(0) == 'n' || v.charAt(0) == 'N') break;
		}
		
		
		//test.addGame(game);
		
		Variables.getVariables().addGame(game);
		
		println("Gra: " + Variables.getVariables().getGames().get(0).getName());
		//println("Gra: " + test.getGames().get(0).getName());
		
	}
	
	public void show() {
		for (Games.Game g: Variables.getVariables().getGames())
		println("Gra: "+g.toString());
		//println("Gra: " + Variables.getVariables().getGames().get(0).getName());
		//println("Gra: " + test.getGames().get(0).getName());
	}
	
	public void end() {
		println("Koniec programu, do zobaczenia!");
		System.exit(0);
	}
	
	private void saveToFIle() {
		try {
			FileOutputStream fos = new FileOutputStream("./baza.txt");
		
			for (Games.Game g: Variables.getVariables().getGames()) {
				Object tmp = g.toString();
				//byte b[] = tmp;
			}
				//byte tmp[] = g.toString();
				//fos.write();
				//println("Gra: "+g.toString());
		}
		catch (Exception ex) {
			
		}
	}
}

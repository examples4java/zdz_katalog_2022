package Games;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Game extends Misc.IO {
	
	private String name, shortName, description;
	private List<String> creators, publishers, genres;
	private Date publishDate;
	private int gameplayTime;
	private String platform;
	
	private Game() {}
	
	private Game(String name) {
		this(name, "");
	}
	
	private Game(String name, String shortName) {
		this.name=name;
		this.shortName=shortName;
		creators=new ArrayList<>();
		publishers=new ArrayList<>();
		genres=new ArrayList<>();
		publishDate=new Date();
	}	
	
	
	public static Game setName(String name) {
		return new Game(name);
	}
	
	public static Game setName(String name, String shortName) {
		return new Game(name, shortName);
	}
	
	public void setShortName(String sn) {
		shortName=sn;
	}
	
	public void setPublishDate(String date) {
		//przemieszczamy si� po ka�dym elemencie storzenego enum
		for (DateRexEx s: DateRexEx.values()) {
			//je�eli data b�dzie pasowa�a
			if (date.matches(s.get())) {
				try {//to zostanie zapisana zgodnie z naszymi preferencjami lokalnymi (np. b�d�c w USA 
					//data zapisze si� notacji ameryka�skiej - powoduje to dodanie Locale.getDefault())
					publishDate = new SimpleDateFormat(s.get(), Locale.getDefault()).parse(date);
				}
				catch (Exception ex) {
					//je�eli daty nie da si� zapisa� - mo�na tutaj wstawi� jaki� kod (domy�lnie nic si� nie dzieje)
				}
			}
		}
		
//		if (date.matches("^[0-9]{2}-[0-9]{2}-[0-9]{4}"))
//			println("Data klasyczna");
//		if (date.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}"))
//			println("Data odwr�cona");
	}
	
	public void setGameplayTime(int time) {
		gameplayTime=time;
	}
	
	public void setGameplayTime(String time) {
		String[] t = null;
		if (time.contains(":"))
			t = time.split(":");
		if (time.contains("."))
			t = time.split(".");
		if (time.contains("-")) 
			t = time.split("-");
		try {
			gameplayTime = Integer.valueOf(t[0])*60+Integer.valueOf(t[1]);
		}
		catch (Exception ex) {
			gameplayTime=0;
		}
	}
	
	public void setPlatform(String name) {
		platform=name;
	}
	
	public void addCreator(String name) {
		if (creators==null) {
			creators = new ArrayList<>();
		}
		creators.add(name);
	}
	
	public void addPublisher(String name) {
		if (publishers==null) {
			publishers = new ArrayList<>();
		}
		publishers.add(name);
	}
	
	public void addGenre(String name) {
		if (genres==null) {
			genres = new ArrayList<>();
		}
		genres.add(name);
	}
	
	public void setDescription(String name) {
		description=name;
	}
	
	public String getName() {
		return name;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getCreators() {
		String ret="";
		for (String c: creators) {
			ret+=c+", ";
		}
		return ret.substring(0,ret.length()-2);
	}
	
	public String getPublishers() {
		String ret="";
		for (String c: publishers) {
			ret+=c+", ";
		}
		return ret.substring(0,ret.length()-2);
	}
	
	public String getGenres() {
		String ret="";
		for (String c: genres) {
			ret+=c+", ";
		}
		return ret.substring(0,ret.length()-2);
	}
	
	public String getPublishDate() {
		return publishDate.toString();
	}
	
	public int getGameplayTime() {
		return gameplayTime;
	}
	
	public String getPlatform() {
		return platform;
	}
	
	public String toString() {
		String s= name + ";" + shortName + ";" + description + ";";
		for(String t: creators)
			s+=t+";";
		for(String t:publishers) 
			s+=t+";";
		for(String t:genres)
			s+=t+";";
		s+=getPublishDate()+";"+description+";"+platform+";"+gameplayTime+"\n";
		return s;
	}
}

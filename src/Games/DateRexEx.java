package Games;
/*
 * Elementy enum pozwalaj� tworzy� list� warto�ci, kt�re nast�pnie mo�na wykorzystywa� w spos�b 
 * podobny do indeksowania. Generalnie nazwy w typie wyliczeniowym zwyk�o si� pisa� du�ymi
 * literami. Poni�sza enumeracja nie jest "typow�" dla Java - enumreacje domy�lnie nie zapisuja
 * pod nazwami konkretnych warto�ci. W naszym wypadku ka�da z nazw jest niejako utworzeniem
 * obiektu z okre�lon� warto�ci�. 
 * 
 * Sam konstruktor w tym wypadku jest private. Dzi�ki temu nikt nie utworzy nowej warto�ci enum
 * (dowolnej, innej ni� zaplanowa� stw�rca enum).
 * 
 */
public enum DateRexEx {
	FULL("^[0-9]{2}[-.]{1}[0-9]{2}[-.]{1}[0-9]{4}"),
	REVERSE("^[0-9]{4}[-.]{1}[0-9]{2}[-.]{1}[0-9]{2}"),
	SLIM("^[0-9]{2}[-./]{1}[0-9]{2}[-./]{1}[0-9]{2}"),
	US("^[0-9]{2}/[0-9]{2}/[0-9]{4}"),
	USREVERSE("^[0-9]{4}/[0-9]{2}/[0-9]{2}");

	//zmienna otrzymuje warto�� w konstruktorze, dlatego tutaj poprawnie zadzia�a 
	//modyfikator final
	private final String v;
	
	private DateRexEx(String string) {
		v=string;
	}
	//metoda wybierze z naszego enum przechowywan� warto��. 
	public final String get() {return v;}
}

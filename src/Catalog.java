
import Controller.Variables;
import Misc.Menu;

/*
 * Nalezy stworzy� program pozwalaj�cy na katalogowanie w�asnej kolekcji gier.

Program musi mie� nast�puj�ce mo�liwo�ci:
 - dodanie nowej gry
 - modyfikacj� opcji wskazanej gry
 - usuni�cie okre�lonej gry
 - wy�wietlenie opisu wskazanej gry
 - wyj�cie z programu
 
 Sama gra powinna mie� nast�puj�ce informacje:
 - nazwa
 - tw�rcy
 - wydawcy
 - gatunki
 - opis
 - data wydania
 - orientacyjny czas przej�cia
 - platforma, na kt�rej dost�pna jest gra
 
 kontakt piotr_dobosz@int.pl
 */

public class Catalog {
	
	/*
	 * Zdefiniowana tutaj klasa g��wna projektu
	 * Catalog tak naprawd� ma tylko jeden cel - wywo�a� menu 
	 * programu. Wszelkie pozosta�e elementy programu b�d�
	 * wywo�ywane w�a�nie przez menu, za� funkcja main 
	 * ma nie pe�ni� �adnych istotnych r�l. W zasadzie ma za 
	 * zadanie stworzy� nowy obiekt i zako�czy� swoje dzia�anie.
	 * 
	 * Takie podej�cie powoduje, �e aby stworzy� zmienn� b�dz
	 * zmienne, kt�re mog�oyby by� wykorzystywane przez inne 
	 * klasy/obiekty musieliby�my stworzy� zmienn� przed wywo�aniem
	 * menu i przekazywa� tak utworzon� zmienn� do kolejnych
	 * konstruktor�w klas wywo�ywanych w kolejnych liniach kodu.
	 * 
	 * Jednak Java ma pewien problem - w przeciwie�stwie do np.
	 * C# lub C/C++ Java przekazuje parametry (zmienne do 
	 * funkcji/metod/konstruktor�w) jako WARTO�CI, nie za� 
	 * WSKA�NIKI. Oznacza to, �e dany obiekt, je�eli nie u�yjemy
	 * pewnego fortelu, b�dzie modyfikowa� kopi� naszej przekazanej
	 * zmiennej i w zasadzie nie zmieni jej globalnie, tj. �adna
	 * inna klasa/obiekt nie zobacz� zaktualizwoanej wersji naszej
	 * zmiennej.
	 * 
	 * Fortelem wspomnianym wy�ej jej przekazanie jako parametru
	 * ca�ego utworzonego ju� wcze�niej obiektu. Takie przekazanie
	 * powoduje, �e przekazujemy jako warto�� numer kom�rki,
	 * pod kt�rym znajduje si� obiekt, a tym samym modyfikujemy
	 * zawarto�� pod adresem dla danych warto�ci. Jednak 
	 * w szczeg�lnych okoliczno�ciach i ten spos�b mo�e okaza� si�
	 * niewystarczaj�cy.
	 * 
	 * Dlatego zaproponowany zosta� inny spos�b przekazania zmiennych
	 * opisany w klasie Variables.
	 */
	public static void main(String[] args) {
		//ta zmienna by�aby do�� uci��liwa w dalszym przekazywaniu
		//do kolejnych instancji dlatego ostatecznie ten 
		//rodzaj zapisu zmiennych nie b�dzie wykorzystany w 
		//programie..
		//WSZYSTKIE odwo�ania do tej zmiennej pozosta�y ale s�
		//zakomentowane (mo�na je przywr�ci� celem sprawdzenia
		//dzia�ania).
		//Variables test = new Variables();
		new Menu(/*test*/).createMenu();	
	}

}
